<?php
/**
 * 2048 Rappi Game Test
 * This is a replica of the famous 2048 game
 * @author Edwin Moedano edwinmoedano@gmail.com
 *
*/
class Piece{

  public $value;
  public $hasSum = false;
  /**
   * Constructor
   * Initialize the Class Piece
   * @param (integer) (value) set the Piece Value
  */
  function __construct($value = 0) {
    $this->value = $value;
  }
  /**
   * isEmpty
   * Check if the Piece is empty
   * @return (boolean)
  */
  function isEmpty(){
    return $this->value === 0;
  }
  /**
   * setValue
   * Assign a value to a piece
   * @param (integer) (value)
  */
  function setValue($value){
    $this->value = $value;
  }
  /**
   * setEmpty
   * Set the value of a Piece to empty and set the flag of sum to false
  */
  function setEmpty(){
    $this->value = 0;
    $this->hasSum = false;
  }
  /**
   * Sum
   * Adds to the actual piece the value of another one and set the flag to avoid being summed again
   * @param (Object Piece) (piece)
  */
  function sum($piece){
    $this->value = $this->value + $piece->value;
    $this->hasSum = true;
  }
}
/**
 * initializeGameArray
 * Initialize the Game Array by a dimension given and set some random Pieces
 * @param (integer) (x) number of rows
 * @param (integer) (y) number of cloumns
 * @param (integer) (randomPieces) number of random piences to add to the Game Array
 * @return (array)
*/
function initializeGameArray($x, $y, $randomPieces){
  $gameArray = array();
  for($i = 0; $i < $x; $i++){
    $gameArray[] = array();
    for($j = 0; $j < $y; $j++)
      $gameArray[$i][] = new Piece;
  }
  if(!insertRandomPieces($gameArray, 2)){
    print_r("Error:There is no space avaible to insert the initial pieces, make the Game Array size bigger. \n");
    return null;
  }
  return $gameArray;
}
/**
 * printGameArray
 * Prints the Game Array
 * @param (array) (gameArray) The Game Array
*/
function printGameArray($gameArray){
  foreach($gameArray as $row){
    foreach ($row as $column) {
      print_r($column->value."  ");
    }
    print_r("\n");
  }
}
/**
 * insertRandomPieces
 * Insert some random pieces to the Game Array, if the function cannot insert a random piece returns false
 * @param (array) (gameArray) The Game Array
 * @param (integer) (numberOfPieces) Number of pieces
 * @return (bool)
*/
function insertRandomPieces(&$gameArray, $numberOfPieces){
  //Check the Game Array if there is enought place to play
  if(($rows = sizeOf($gameArray)) > 0 && ($columns = sizeOf($gameArray[0])) > 0){ //&& ( > 0)
    //Init the pieces inserted counter
    $piecesInserted = 0;
    //Insert the number Of Random Pieces requested
    for($i = 0; $i < $numberOfPieces; $i++){
      //If there is some free space to insert a piece do it
      if(hasFreeSpace($gameArray)){
        //As long as we are able to insert this piece calculate random Colum and Row to insert the piece
        while(true){
          $randomRow = mt_rand(0,$rows - 1);
          $randomColumn = mt_rand(0,$columns - 1);
          //Check if the coordinate is empty and insert the piece
          if($gameArray[$randomRow][$randomColumn]->isEmpty()){
            //Increment the pieces counter
            $piecesInserted++;
            //Insert the piece
            //TODO:Change it with a random númber pow of 2
            $gameArray[$randomRow][$randomColumn] = new Piece(2);
            break;
          }
        }
      }else{
        print_r("Warning:The Game Array is full, we can't insert more pieces.\n");
        break;
      }
    }
    return $piecesInserted === $numberOfPieces;
  }
  print_r("Error:The Game Array is empty, configure a game array with rows and columns greater than 0.\n");
  return false;
}
/**
 * hasFreeSpace
 * Check in the GameArray if we have space to insert a random Piece
 * @param (array) (gameArray) The Game Array
 * @return (bool)
*/
function hasFreeSpace($gameArray){
  foreach($gameArray as $row){
    foreach ($row as $column) {
      if($column->value == 0)
        return true;
    }
  }
  return false;
}
/**
 * moveGameArrayDown
 * Move all the pieces of the GameArray DOWN
 * @param (array) (gameArray) The Game Array
*/
function moveGameArrayDown(&$gameArray){
  for($i = sizeOf($gameArray) - 1; $i >= 0; $i--){
    for($j = 0; $j <= sizeOf($gameArray[$i]) - 1; $j++){
      //Check if the actual piece is not empty and the pointer is not in the end
      if(!$gameArray[$i][$j]->isEmpty() && $i < (sizeOf($gameArray) - 1)){
        //Check if the actual piece is different from the piece downside
        if($gameArray[$i][$j]->value != $gameArray[$i +1][$j]->value){
          //Check if the downside piece is empty
          if($gameArray[$i + 1][$j]->isEmpty()){
            $gameArray[$i + 1][$j]->setValue($gameArray[$i][$j]->value);
            $gameArray[$i][$j]->setEmpty();
            $i = $i + 2;
            break;
          }
        }elseif(!$gameArray[$i + 1][$j]->hasSum){
          $gameArray[$i + 1][$j]->sum($gameArray[$i][$j]);
          $gameArray[$i][$j]->setEmpty();
        }
      }
    }
    if($i > sizeOf($gameArray) - 1){$i--;}
    //The row was finished, clear the hasSum flag to all the pieces
    for($j = 0; $j <= sizeOf($gameArray[$i]) - 1; $j++)
      $gameArray[$i][$j]->hasSum = false;
  }
}
/**
 * moveGameArrayUp
 * Move all the pieces of the GameArray UP
 * @param (array) (gameArray) The Game Array
*/
function moveGameArrayUp(&$gameArray){
  for($i = 0; $i < sizeOf($gameArray); $i++){
    for($j = 0; $j <= sizeOf($gameArray[$i]) - 1; $j++){
      //Check if the actual piece is not empty and the pointer is not in the end
      if(!$gameArray[$i][$j]->isEmpty() && $i > 0){
        //Check if the actual piece is different from the piece a upside
        if($gameArray[$i][$j]->value != $gameArray[$i - 1][$j]->value){
          //Check if the upside piece is empty
          if($gameArray[$i - 1][$j]->isEmpty()){
            $gameArray[$i - 1][$j]->setValue($gameArray[$i][$j]->value);
            $gameArray[$i][$j]->setEmpty();
            $i = $i - 2;
            break;
          }
        }elseif(!$gameArray[$i - 1][$j]->hasSum){
          $gameArray[$i - 1][$j]->sum($gameArray[$i][$j]);
          $gameArray[$i][$j]->setEmpty();
        }
      }
    }
    if($i < 0){$i = 0;}
    //The row was finished, clear the hasSum flag to all the pieces
    for($j = sizeOf($gameArray[$i]) - 1; $j >= 0; $j--)
      $gameArray[$i][$j]->hasSum = false;
  }
}
/**
 * moveGameArrayRight
 * Move all the pieces of the GameArray RIGHT
 * @param (array) (gameArray) The Game Array
*/
function moveGameArrayRight(&$gameArray){
  for($i = 0; $i < sizeOf($gameArray); $i++){
    for($j = sizeOf($gameArray[$i]) - 1; $j >= 0; $j--){
      //Check if the actual piece is not empty and the pointer is not in the end
      if(!$gameArray[$i][$j]->isEmpty() && $j < (sizeOf($gameArray[$i]) - 1)){
        //Check if the actual piece is different from the piece a side
        if($gameArray[$i][$j]->value != $gameArray[$i][$j + 1]->value){
          //Check if the next piece is empty
          if($gameArray[$i][$j + 1]->isEmpty()){
            $gameArray[$i][$j + 1]->setValue($gameArray[$i][$j]->value);
            $gameArray[$i][$j]->setEmpty();
            $j = $j + 2;
          }
        }elseif(!$gameArray[$i][$j + 1]->hasSum){
          $gameArray[$i][$j + 1]->sum($gameArray[$i][$j]);
          $gameArray[$i][$j]->setEmpty();
        }
      }
    }
    //The row was finished, clear the hasSum flag to all the pieces
    for($j = sizeOf($gameArray[$i]) - 1; $j >= 0; $j--)
      $gameArray[$i][$j]->hasSum = false;
  }
}
/**
 * moveGameArrayLeft
 * Move all the pieces of the GameArray LEFT
 * @param (array) (gameArray) The Game Array
*/
function moveGameArrayLeft(&$gameArray){
  for($i = 0; $i < sizeOf($gameArray); $i++){
    for($j = 0; $j <= sizeOf($gameArray[$i]) - 1; $j++){
      //Check if the actual piece is not empty and the pointer is not in the end
      if(!$gameArray[$i][$j]->isEmpty() && $j > 0){
        //Check if the actual piece is different from the piece a side
        if($gameArray[$i][$j]->value != $gameArray[$i][$j - 1]->value){
          //Check if the next piece is empty
          if($gameArray[$i][$j - 1]->isEmpty()){
            $gameArray[$i][$j - 1]->setValue($gameArray[$i][$j]->value);
            $gameArray[$i][$j]->setEmpty();
            $j = $j - 2;
          }
        }elseif(!$gameArray[$i][$j - 1]->hasSum){
          $gameArray[$i][$j - 1]->sum($gameArray[$i][$j]);
          $gameArray[$i][$j]->setEmpty();
        }
      }
    }
    //The row was finished, clear the hasSum flag to all the pieces
    for($j = 0;$j <= sizeOf($gameArray[$i]) - 1; $j++)
      $gameArray[$i][$j]->hasSum = false;
  }
}
/**
 * moveGameArray
 * This function check the command typed and executes the right move for the Game Array
 * @param (array) (gameArray) The Game Array
 * @param (string) (command) The command to execute could be abajo, arriba, derecha o izquierda.
*/
function moveGameArray(&$gameArray, $command){
  //Derecha
  if($command == 'derecha'){
    moveGameArrayRight($gameArray);
  }elseif($command == 'izquierda'){
    moveGameArrayLeft($gameArray);
  }elseif($command == 'arriba'){
    moveGameArrayUp($gameArray);
  }elseif($command == 'abajo'){
    moveGameArrayDown($gameArray);
  }
  //Insert a new Piece to the Game
  insertRandomPieces($gameArray, 1);
  printGameArray($gameArray);
}
/**
 * initializeGame
 * This function initialize the game
*/
function initializeGame(){
  $commands = array('');
  $validCommands = array('derecha','izquierda','arriba','abajo');
  //Initialize the Game Array with 4X4 dimensions and 2 random Pieces
  $gameArray = initializeGameArray(4,4,2);
  if($gameArray){
    //Print the Game Array
    printGameArray($gameArray);
    while($commands[0] != 'exit'){
      $moves = intval(readline("Moves: "));
      if($moves > 0){
        $commands = array();
        for($i = 0;$i < $moves; $i++){
          $commands[] = strtolower(readline("Command: "));
        }
        for($i = 0;$i < sizeOf($commands); $i++){
          if(in_array($commands[$i], $validCommands)){
            moveGameArray($gameArray, $commands[$i]);
            print_r("\n");
          }elseif($commands[$i] == 'exit'){
            //TODO:Maybe print the final score?
            break;
          }else{
            print_r("Warning: ".$commands[$i]." command not found \n");
          }
        }
      }else{
        print_r("Warning: The number of moves must be at least one!\n");
      }
    }
  }
}

initializeGame();
